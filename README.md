INTRODUCTION
------------

This module is an extension to the [Group](https://www.drupal.org/project/group)
module that allows you to hide group roles from the user interface.

The module adds a 'Hidden role' checkbox on the group role edit form. Roles with
'Hidden role' option enabled will be not displayed in the group member's list of
roles and will be removed from the assignable roles list when adding or editing
a member.

Assigning a hidden role can only be done in code or by temporarily unchecking
the 'Hidden role' option and enabling it again after assignment.

REQUIREMENTS
------------

 - Group module (https://drupal.org/project/group) version 8.x-1.x

INSTALLATION
------------

Install the Group Hidden Role module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

MAINTAINERS
------------

- Péter Keszthelyi - https://www.drupal.org/u/keszthelyi

Supporting organizations:

- European Commission - https://www.drupal.org/european-commission
