<?php

declare(strict_types = 1);

namespace Drupal\group_hidden_role\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\group\Plugin\EntityReferenceSelection\GroupTypeRoleSelection;

/**
 * Shows group roles available for a group type, filtering out hidden ones.
 *
 * @EntityReferenceSelection(
 *   id = "group_type:group_role_exclude_hidden",
 *   label = @Translation("Group type role selection (hidden roles excluded)"),
 *   entity_types = {"group_role"},
 *   group = "group_type",
 *   weight = 1
 * )
 */
class GroupTypeRoleExcludeHiddenSelection extends GroupTypeRoleSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS'): QueryInterface {
    $query = parent::buildEntityQuery($match, $match_operator);
    $or_group = $query->orConditionGroup()
      ->notExists('third_party_settings.group_hidden_role.hidden_role')
      ->condition('third_party_settings.group_hidden_role.hidden_role', 1, '<>');
    $query->condition($or_group);

    return $query;
  }

}
