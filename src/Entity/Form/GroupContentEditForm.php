<?php

declare(strict_types = 1);

namespace Drupal\group_hidden_role\Entity\Form;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\Form\GroupContentForm;

/**
 * Form handler for group content edit forms.
 */
class GroupContentEditForm extends GroupContentForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
    $group_content = $form_state->getFormObject()->getEntity();

    if ($group_content->getContentPlugin()->getPluginId() === 'group_membership') {
      $submitted_roles = $form_state->getValue('group_roles');
      foreach ($group_content->group_roles as $role_item) {
        assert($role_item instanceof EntityReferenceItem);
        if (
          ($role = $role_item->entity) &&
          $role->getThirdPartySetting('group_hidden_role', 'hidden_role') &&
          !in_array($role_item->target_id, array_column($submitted_roles, 'target_id'))
        ) {
          // If the group membership has hidden roles that were not submitted
          // with the form, add them to the submitted roles.
          $submitted_roles[] = ['target_id' => $role_item->target_id];
        }
      }
      $form_state->setValue('group_roles', $submitted_roles);
    }

    parent::submitForm($form, $form_state);
  }

}
